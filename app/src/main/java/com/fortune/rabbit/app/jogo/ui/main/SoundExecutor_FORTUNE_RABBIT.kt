package com.fortune.rabbit.app.jogo.ui.main

import android.content.Context
import android.media.MediaPlayer
import com.fortune.rabbit.app.jogo.R

object SoundExecutor_FORTUNE_RABBIT {

    private var mediaPlayerFortuneRabbit: MediaPlayer? = null

    fun startWork_FORTUNE_RABBIT(context: Context) {
        mediaPlayerFortuneRabbit = MediaPlayer.create(context.applicationContext, R.raw.sound)
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }

    fun startProcessing_FORTUNE_RABBIT() {
        mediaPlayerFortuneRabbit?.start()
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }
}
