package com.fortune.rabbit.app.jogo.ui.plug.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material.Slider
import androidx.compose.material.SliderDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.main.RabbitEventListener
import com.fortune.rabbit.app.jogo.ui.main.MainActivity_FORTUNE_RABBIT
import com.fortune.rabbit.app.jogo.ui.main.RabbitBackPhoto
import com.fortune.rabbit.app.jogo.ui.plug.viewmodel.OptionsViewModel_FORTUNE_RABBIT
import com.fortune.rabbit.app.jogo.ui.theme.MyPurpleFortuneRabbit

@Composable
fun OptionScreen_FORTUNE_RABBIT(
    navControllerRabbitFortune: NavController,
    optionsViewModelFORTUNERABBIT: OptionsViewModel_FORTUNE_RABBIT,
) {
    val activityRabbitFortune = LocalContext.current as MainActivity_FORTUNE_RABBIT
    val settingsRabbitFortune by optionsViewModelFORTUNERABBIT.userOptions.collectAsState()
    RabbitBackPhoto(
        R.drawable.options_background
    )
    Box(
        modifier = Modifier.fillMaxSize(),
        contentAlignment = Alignment.Center
    ) {
        Column(
            modifier = Modifier.fillMaxWidth().offset(y = 40.dp),
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.spacedBy(32.dp, Alignment.CenterVertically)
        ) {
            ShirtStarter_FORTUNE_RABBIT(
                photoIdFortuneRabbit = R.drawable.sound,
                valueFortuneRabbit = settingsRabbitFortune?.get(0) ?: 0f,
            ) {
                optionsViewModelFORTUNERABBIT.setConfigurations(1, it)
            }
            ShirtStarter_FORTUNE_RABBIT(
                photoIdFortuneRabbit = R.drawable.music,
                valueFortuneRabbit = settingsRabbitFortune?.get(1) ?: 0f,
            ) {
                if (it > 0f) {
                    activityRabbitFortune.initMusicOnBackground()
                } else {
                    activityRabbitFortune.pauseMusicOnBackFortuneRabbit()
                }
                optionsViewModelFORTUNERABBIT.setConfigurations(2, it)
            }
        }
        RabbitEventListener(
            photoFortuneRabbit = R.drawable.back_button,
            isNeedToClickWithSoundFortuneRabbit = (settingsRabbitFortune?.get(0) ?: 0f) > 0f,
            modificatorFortuneRabbit = Modifier
                .width(32.dp)
                .align(Alignment.TopEnd)
                .offset((-42).dp)
        ) {
            navControllerRabbitFortune.popBackStack()
        }
    }
}

@Composable
fun ShirtStarter_FORTUNE_RABBIT(
    photoIdFortuneRabbit: Int,
    valueFortuneRabbit: Float,
    onClickFortuneRabbit: (Float) -> Unit
) {
    var stateRabbitFortune by remember { mutableStateOf(valueFortuneRabbit) }
    Row(
        modifier = Modifier
            .fillMaxWidth(.8f),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween
    ) {
        RabbitEventListener(
            modificatorFortuneRabbit = Modifier.size(56.dp),
            photoFortuneRabbit = photoIdFortuneRabbit,
            isNeedToClickWithSoundFortuneRabbit = false
        ) {}
        Slider(
            value = stateRabbitFortune,
            onValueChange = {
                stateRabbitFortune = it
            },
            modifier = Modifier.fillMaxWidth(.85f),
            steps = 5,
            onValueChangeFinished = {
                onClickFortuneRabbit.invoke(stateRabbitFortune)
            },
            colors = SliderDefaults.colors(
                thumbColor = MyPurpleFortuneRabbit,
                disabledActiveTickColor = MyPurpleFortuneRabbit,
                disabledActiveTrackColor = MyPurpleFortuneRabbit,
                disabledInactiveTickColor = MyPurpleFortuneRabbit,
                disabledInactiveTrackColor = MyPurpleFortuneRabbit,
                disabledThumbColor = MyPurpleFortuneRabbit,
                activeTrackColor = MyPurpleFortuneRabbit,
                inactiveTrackColor = MyPurpleFortuneRabbit,
                activeTickColor = MyPurpleFortuneRabbit,
                inactiveTickColor = MyPurpleFortuneRabbit
            )
        )
    }
    LaunchedEffect(key1 = true) {
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }
}
