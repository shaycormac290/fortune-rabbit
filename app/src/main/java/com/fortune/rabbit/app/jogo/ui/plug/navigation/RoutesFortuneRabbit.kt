package com.fortune.rabbit.app.jogo.ui.plug.navigation

sealed class RoutesFortuneRabbit(val routeFortuneRabbit: String) {

    class StartRoute: RoutesFortuneRabbit("start_screen_FORTUNE_RABBIT")
    class RabbitFirstStartRoute: RoutesFortuneRabbit("first_start_screen_FORTUNE_RABBIT")
    class ProgressRabbitRoute: RoutesFortuneRabbit("loading_screen_FORTUNE_RABBIT")
    class RabbitOptionRoute: RoutesFortuneRabbit("option_screen_FORTUNE_RABBIT")
    class RabbitGameRoute: RoutesFortuneRabbit("game_screen_FORTUNE_RABBIT")
    class RabbitWinRoute: RoutesFortuneRabbit("win_screen_FORTUNE_RABBIT")
    class RabbitDoYouWannaExitRoute: RoutesFortuneRabbit("wanna_exit_screen_FORTUNE_RABBIT")

    class OneFortuneRabbit: RoutesFortuneRabbit("one_screen_FORTUNE_RABBIT")
    class TwoFortuneRabbit: RoutesFortuneRabbit("one_screen_FORTUNE_RABBIT")
    class ThreeFortuneRabbit: RoutesFortuneRabbit("one_screen_FORTUNE_RABBIT")
    class FourFortuneRabbit: RoutesFortuneRabbit("one_screen_FORTUNE_RABBIT")

}
