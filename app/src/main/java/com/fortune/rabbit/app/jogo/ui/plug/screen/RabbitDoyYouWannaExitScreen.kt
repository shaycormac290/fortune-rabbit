package com.fortune.rabbit.app.jogo.ui.plug.screen

import android.app.Activity
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.main.RabbitEventListener
import com.fortune.rabbit.app.jogo.ui.main.SoundExecutor_FORTUNE_RABBIT
import com.fortune.rabbit.app.jogo.ui.main.RabbitBackPhoto

@Composable
fun RabbitDoyYouWannaExitScreen(
    navControllerRabbitFortune: NavController,
    settingsRabbitFortune: List<Float>?,
    contextRabbitFortune: Activity
) {
    RabbitBackPhoto(
        R.drawable.do_you_wanna_exit
    )
    Row(
        modifier = Modifier.fillMaxSize(),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceEvenly
    ) {
        ExitButton(idFortuneRabbit = R.drawable.yes, buttonIdFortuneRabbit = R.drawable.yes_icon) {
            if ((settingsRabbitFortune?.get(0) ?: 0f) > 0f) {
                SoundExecutor_FORTUNE_RABBIT.startProcessing_FORTUNE_RABBIT()
            }
            contextRabbitFortune.finish()
        }
        ExitButton(idFortuneRabbit = R.drawable.no, buttonIdFortuneRabbit = R.drawable.no_button) {
            if ((settingsRabbitFortune?.get(0) ?: 0f) > 0f) {
                SoundExecutor_FORTUNE_RABBIT.startProcessing_FORTUNE_RABBIT()
            }
            navControllerRabbitFortune.popBackStack()
        }
    }
    LaunchedEffect(key1 = true) {
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }
}

@Composable
fun ExitButton(
    idFortuneRabbit: Int,
    buttonIdFortuneRabbit: Int,
    onClickFortuneRabbit: () -> Unit
) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.spacedBy(24.dp, Alignment.CenterVertically)
    ) {
        Image(
            painter = painterResource(id = idFortuneRabbit),
            contentDescription = null,
        )
        RabbitEventListener(
            photoFortuneRabbit = buttonIdFortuneRabbit,
            isNeedToClickWithSoundFortuneRabbit = false,
            modificatorFortuneRabbit = Modifier.width(48.dp)
        ) {
            onClickFortuneRabbit.invoke()
        }
    }
}