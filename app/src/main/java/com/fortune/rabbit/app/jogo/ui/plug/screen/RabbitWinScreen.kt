package com.fortune.rabbit.app.jogo.ui.plug.screen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.offset
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.main.RabbitEventListener
import com.fortune.rabbit.app.jogo.ui.main.RabbitBackPhoto
import com.fortune.rabbit.app.jogo.ui.main.AnnotatedRabbitText
import com.fortune.rabbit.app.jogo.ui.plug.viewmodel.GameViewModel_FORTUNE_RABBIT
import com.fortune.rabbit.app.jogo.ui.theme.MyGreenFortuneRabbit

@Composable
fun RabbitWinScreen(
    navControllerFortuneRabbit: NavController,
    gameViewModelFORTUNERABBIT: GameViewModel_FORTUNE_RABBIT
) {
    val neededSumFortuneRabbit by gameViewModelFORTUNERABBIT.points.collectAsState()
    RabbitBackPhoto(
        R.drawable.win_background
    )
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Box(modifier = Modifier
            .fillMaxWidth()
            .padding(24.dp)) {
            AnnotatedRabbitText(
                stringFortuneRabbit = "${stringResource(R.string.your_balance)}\n${neededSumFortuneRabbit}",
                backFortuneRabbit = MyGreenFortuneRabbit,
                fontAmount = 22.sp,
                aligningFortuneRabbit = TextAlign.Center,
                modifierFortuneRabbit = Modifier
                    .align(Alignment.Center)
                    .offset(y = 24.dp)
            )
            RabbitEventListener(
                photoFortuneRabbit = R.drawable.back_button,
                isNeedToClickWithSoundFortuneRabbit = false,
                modificatorFortuneRabbit = Modifier
                    .width(32.dp)
                    .align(Alignment.CenterEnd)
                    .offset(y = (-42).dp)
            ) {
                navControllerFortuneRabbit.popBackStack()
            }
        }
    }
}