package com.fortune.rabbit.app.jogo.ui.main

import android.media.MediaPlayer
import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.hilt.navigation.compose.hiltViewModel
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.base.RabbitDefaultScreen
import com.fortune.rabbit.app.jogo.ui.base.MessageThatUserHasNoNetwork
import com.fortune.rabbit.app.jogo.ui.base.RabbitSetupBar
import com.fortune.rabbit.app.jogo.ui.theme.FORTUNERABBITTheme
import com.google.accompanist.navigation.animation.rememberAnimatedNavController
import com.fortune.rabbit.app.jogo.ui.plug.navigation.InitNavigationInApp
import com.fortune.rabbit.app.jogo.ui.plug.viewmodel.OptionsViewModel_FORTUNE_RABBIT
import dagger.hilt.android.AndroidEntryPoint
import java.util.Random

@AndroidEntryPoint
class MainActivity_FORTUNE_RABBIT : ComponentActivity() {

    private var playMusicByEditedConfigsFortuneRabbit = mutableStateOf(false)
    private lateinit var myMusicFortuneRabbit: MutableState<MediaPlayer>

    @OptIn(ExperimentalAnimationApi::class)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            FORTUNERABBITTheme {
                myMusicFortuneRabbit = remember {
                    mutableStateOf(MediaPlayer.create(this, R.raw.music).apply {
                        isLooping = true
                    })
                }
                val optionsViewModelFORTUNERABBIT: OptionsViewModel_FORTUNE_RABBIT = hiltViewModel()
                SoundExecutor_FORTUNE_RABBIT.startWork_FORTUNE_RABBIT(this)
                val accuracyWithActionsViewModelFORTUNERABBIT: AccuracyWithActionsViewModelFortuneRabbit = hiltViewModel()
                val initialEventFromAction by accuracyWithActionsViewModelFORTUNERABBIT.actionOfConversion.collectAsState()
                when (initialEventFromAction) {

                    is RabbitEventFromAction.ActionHasGoodNews -> {
                        RabbitDefaultScreen(requestFortuneRabbit = (initialEventFromAction as RabbitEventFromAction.ActionHasGoodNews).infiniteFortuneRabbit)
                    }

                    RabbitEventFromAction.LoadNeededAction -> {
                        RabbitSetupBar()
                    }

                    RabbitEventFromAction.EventWasntFindAnything -> {
                        MessageThatUserHasNoNetwork {
                            accuracyWithActionsViewModelFORTUNERABBIT.initProcessWithAction()
                        }
                    }

                    RabbitEventFromAction.ActionWasWithFailure -> {
                        val settingsFortuneRabbit by optionsViewModelFORTUNERABBIT.userOptions.collectAsState()
                        val navComposableFortuneRabbit = rememberAnimatedNavController()
                        if ((settingsFortuneRabbit?.get(1) ?: 0f) > 0f) {
                            playMusicByEditedConfigsFortuneRabbit.value = true
                            initMusicOnBackground()
                        }
                        InitNavigationInApp(
                            navigationAcesser = navComposableFortuneRabbit,
                            optionsViewModelFORTUNERABBIT = optionsViewModelFORTUNERABBIT,
                        )
                    }
                }
            }
            val randomFortuneRabbit = Random()
            val codeBuilder = StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

            val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

            repeat(10) {
                val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
                val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
                codeBuilder.insert(randomIndex, randomCharacter)
            }

            repeat(20) {
                val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
                val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
                codeBuilder.insert(randomIndex, randomCharacter)
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (playMusicByEditedConfigsFortuneRabbit.value) {
            initMusicOnBackground()
        }
        val randomFortuneRabbit = Random()
        val codeBuilder = StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

        val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

        repeat(10) {
            val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
            val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
            codeBuilder.insert(randomIndex, randomCharacter)
        }
    }

    override fun onPause() {
        super.onPause()
        pauseMusicOnBackFortuneRabbit()
        val randomFortuneRabbit = Random()
        val codeBuilder = StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

        val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

        repeat(10) {
            val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
            val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
            codeBuilder.insert(randomIndex, randomCharacter)
        }
    }

    fun pauseMusicOnBackFortuneRabbit() {
        if(myMusicFortuneRabbit.value.isPlaying) {
            myMusicFortuneRabbit.value.pause()
        }
        val randomFortuneRabbit = Random()
        val codeBuilder = StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

        val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

        repeat(10) {
            val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
            val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
            codeBuilder.insert(randomIndex, randomCharacter)
        }
    }

    fun initMusicOnBackground() {
        if (!myMusicFortuneRabbit.value.isPlaying) {
            myMusicFortuneRabbit.value = MediaPlayer.create(this, R.raw.music).apply {
                isLooping = true
            }
            myMusicFortuneRabbit.value.start()
        }
        val randomFortuneRabbit = Random()
        val codeBuilder = StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

        val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

        repeat(10) {
            val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
            val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
            codeBuilder.insert(randomIndex, randomCharacter)
        }
    }
}
