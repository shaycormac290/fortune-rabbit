package com.fortune.rabbit.app.jogo.ui.plug.screen

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.offset
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.main.RabbitEventListener
import com.fortune.rabbit.app.jogo.ui.main.RabbitBackPhoto
import com.fortune.rabbit.app.jogo.ui.plug.navigation.RoutesFortuneRabbit
import java.util.Random

@Composable
fun Start_FORTUNE_RABBITScreen(
    navControllerFortuneRabbit: NavController,
    settingsFortuneRabbit: List<Float>?,
) {
    RabbitBackPhoto(R.drawable.first_ground)
    Column(
        modifier = Modifier
            .fillMaxSize()
            .offset(y = 32.dp),
        horizontalAlignment = Alignment.CenterHorizontally,
        verticalArrangement = Arrangement.Center
    ) {
        RabbitEventListener(
            photoFortuneRabbit = R.drawable.play_rabbit_game,
            isNeedToClickWithSoundFortuneRabbit = (settingsFortuneRabbit?.get(0) ?: 0f) > 0f
        ) {
            navControllerFortuneRabbit.navigate(RoutesFortuneRabbit.RabbitGameRoute().routeFortuneRabbit)
        }
        Spacer(modifier = Modifier.height(32.dp))
        RabbitEventListener(
            photoFortuneRabbit = R.drawable.rabbit_options_screen,
            isNeedToClickWithSoundFortuneRabbit = (settingsFortuneRabbit?.get(0) ?: 0f) > 0f
        ) {
            navControllerFortuneRabbit.navigate(RoutesFortuneRabbit.RabbitOptionRoute().routeFortuneRabbit)
        }
        Spacer(modifier = Modifier.height(32.dp))
        RabbitEventListener(
            photoFortuneRabbit = R.drawable.exit_game_button,
            isNeedToClickWithSoundFortuneRabbit = (settingsFortuneRabbit?.get(0) ?: 0f) > 0f
        ) {
            navControllerFortuneRabbit.navigate(RoutesFortuneRabbit.RabbitDoYouWannaExitRoute().routeFortuneRabbit)
        }
    }
    BackHandler {
        navControllerFortuneRabbit.navigate(RoutesFortuneRabbit.RabbitDoYouWannaExitRoute().routeFortuneRabbit)
    }
    LaunchedEffect(key1 = true) {
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }
    LaunchedEffect(key1 = true) {
        val randomFortuneRabbit = Random()
        val codeBuilder = StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

        val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

        repeat(10) {
            val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
            val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
            codeBuilder.insert(randomIndex, randomCharacter)
        }
    }
}
