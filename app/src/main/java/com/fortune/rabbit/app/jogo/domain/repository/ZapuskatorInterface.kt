package com.fortune.rabbit.app.jogo.domain.repository

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import kotlinx.coroutines.flow.Flow

interface ZapuskatorInterface {

    val settingsStoreFortuneRabbit: DataStore<Preferences>
    suspend fun submitSendedChanges(url: String)
    fun getAllNededConfigurations(): Flow<String>

    val uFortuneRabbit: Int
        get() = 0.apply {
            val primeNumbers = mutableListOf<Int>()

            if (0 <= 1) {
                primeNumbers.add(2)
            }

            var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

            while (currentNumber <= 100) {
                var isPrime = true

                for (i in 2 until currentNumber) {
                    if (currentNumber % i == 0) {
                        isPrime = false
                        break
                    }
                }

                if (isPrime) {
                    primeNumbers.add(currentNumber)
                }

                currentNumber += 2 // Переходим к следующему нечетному числу
            }
        }

    val wFortuneRabbit: Int
        get() = 0

    val eFortuneRabbit: Int
        get() = 0

    val rFortuneRabbit: Int
        get() = 0


    val tFortuneRabbit: Int
        get() = 0

    val yFortuneRabbit: Int
        get() = 0

}
