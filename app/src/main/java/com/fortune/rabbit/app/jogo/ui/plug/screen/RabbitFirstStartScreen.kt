package com.fortune.rabbit.app.jogo.ui.plug.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.main.SoundExecutor_FORTUNE_RABBIT
import com.fortune.rabbit.app.jogo.ui.main.RabbitBackPhoto
import com.fortune.rabbit.app.jogo.ui.plug.navigation.RoutesFortuneRabbit

@Composable
fun RabbitFirstStartScreen(
    navControllerRabbitFortune: NavController,
    settingsRabbitFortune: List<Float>?
) {
    RabbitBackPhoto(
        R.drawable.rabbit_back
    )
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(64.dp)
    ) {
        Image(
            painter = painterResource(id = R.drawable.start_game),
            contentDescription = null,
            modifier = Modifier
                .align(Alignment.BottomCenter)
                .padding(12.dp)
                .clip(RoundedCornerShape(10))
                .clickable {
                    if ((settingsRabbitFortune?.get(0) ?: 0f) > 0f) {
                        SoundExecutor_FORTUNE_RABBIT.startProcessing_FORTUNE_RABBIT()
                    }
                    navControllerRabbitFortune.run {
                        popBackStack()
                        navigate(RoutesFortuneRabbit.StartRoute().routeFortuneRabbit)
                    }
                },
            contentScale = ContentScale.Fit
        )
    }
    LaunchedEffect(key1 = true) {
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }
}