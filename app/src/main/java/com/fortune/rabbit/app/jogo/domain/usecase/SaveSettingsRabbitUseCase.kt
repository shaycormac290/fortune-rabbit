package com.fortune.rabbit.app.jogo.domain.usecase

import com.fortune.rabbit.app.jogo.domain.repository.ZapuskatorInterface
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class SaveSettingsRabbitUseCase @Inject constructor(
    @Named("Settings") private val repositoryFortuneRabbit: ZapuskatorInterface,
    @Named("anime") private val animeFortuneRabbit: Int,
    private val dispatcherFortuneRabbit: CoroutineDispatcher
) {

    suspend fun workUntilItDie(url: String) =
        withContext(dispatcherFortuneRabbit) {
            println(animeFortuneRabbit * 777)
            repositoryFortuneRabbit.submitSendedChanges(url)
        }

}
