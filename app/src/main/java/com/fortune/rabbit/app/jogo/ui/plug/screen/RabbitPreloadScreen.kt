package com.fortune.rabbit.app.jogo.ui.plug.screen

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Brush
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.main.RabbitBackPhoto
import com.fortune.rabbit.app.jogo.ui.main.AnnotatedRabbitText
import com.fortune.rabbit.app.jogo.ui.plug.navigation.RoutesFortuneRabbit
import com.fortune.rabbit.app.jogo.ui.theme.OrangeFortuneRabbit
import kotlinx.coroutines.delay

@Composable
fun RabbitPreloadScreen(
    navControllerRabbitFortune: NavController,
) {
    RabbitBackPhoto(
        R.drawable.rabbit_back
    )
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(
                Brush.verticalGradient(
                    listOf(
                        Color.Transparent,
                        Color.Black
                    )
                )
            )
    )
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(64.dp),
        contentAlignment = Alignment.BottomCenter
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            LinearProgressIndicator(
                modifier = Modifier.fillMaxWidth(.8f),
                color = OrangeFortuneRabbit,
                backgroundColor = Color.White,
                strokeCap = StrokeCap.Round
            )
            AnnotatedRabbitText(
                stringFortuneRabbit = stringResource(R.string.loading),
                backFortuneRabbit = Color.White,
                fontAmount = 20.sp,
                modifierFortuneRabbit = Modifier.fillMaxWidth(),
                aligningFortuneRabbit = TextAlign.Center
            )
        }
    }
    LaunchedEffect(key1 = true) {
        delay((2000..4000).random().toLong())
        delay(100)
        navControllerRabbitFortune.run {
            popBackStack()
            navigate(RoutesFortuneRabbit.RabbitFirstStartRoute().routeFortuneRabbit)
        }
    }
    LaunchedEffect(key1 = true) {
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }

}
