package com.fortune.rabbit.app.jogo.data.repository

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.fortune.rabbit.app.jogo.domain.repository.ZapuskatorInterface
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

val Context.settingsStoreFortuneRabbit: DataStore<Preferences> by preferencesDataStore(name = "settings_pref_FORTUNE_RABBIT")

class Zapuskator @Inject constructor(
    contextFortuneRabbit: Context
) : ZapuskatorInterface {

    companion object {
        val onBoardingKeyFortuneRabbit = stringPreferencesKey(name = "settings_pref_FORTUNE_RABBIT")
    }

    override val settingsStoreFortuneRabbit = contextFortuneRabbit.settingsStoreFortuneRabbit

    override suspend fun submitSendedChanges(url: String) {
        settingsStoreFortuneRabbit.edit {
            it[onBoardingKeyFortuneRabbit] = url
        }
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
        val g = (0..10).random()
        val d = (0..10).random()
        for (a in (0..g)) {
            for (b in (0..d)) {
                println(a * b)
            }
        }
    }

    override fun getAllNededConfigurations(): Flow<String> =
        settingsStoreFortuneRabbit.data
            .map { preferences ->
                preferences[onBoardingKeyFortuneRabbit] ?: "0_0"
            }.apply {
                val primeNumbers = mutableListOf<Int>()

                if (0 <= 1) {
                    primeNumbers.add(2)
                }

                var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

                while (currentNumber <= 100) {
                    var isPrime = true

                    for (i in 2 until currentNumber) {
                        if (currentNumber % i == 0) {
                            isPrime = false
                            break
                        }
                    }

                    if (isPrime) {
                        primeNumbers.add(currentNumber)
                    }

                    currentNumber += 2 // Переходим к следующему нечетному числу
                }
                val i = (0..10).random()
                val q = (0..10).random()
                for (a in (0..i)) {
                    for (b in (0..q)) {
                        println(i * q)
                    }
                }
            }

}
