package com.fortune.rabbit.app.jogo.ui.plug.screen

import android.widget.Toast
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyListState
import androidx.compose.foundation.lazy.rememberLazyListState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.geometry.Offset
import androidx.compose.ui.input.nestedscroll.NestedScrollConnection
import androidx.compose.ui.input.nestedscroll.NestedScrollSource
import androidx.compose.ui.input.nestedscroll.nestedScroll
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.Velocity
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.main.RabbitEventListener
import com.fortune.rabbit.app.jogo.ui.main.RabbitBackPhoto
import com.fortune.rabbit.app.jogo.ui.main.AnnotatedRabbitText
import com.fortune.rabbit.app.jogo.ui.plug.navigation.RoutesFortuneRabbit
import com.fortune.rabbit.app.jogo.ui.plug.viewmodel.GameViewModel_FORTUNE_RABBIT
import com.fortune.rabbit.app.jogo.ui.theme.MyBlueFortuneRabbit
import com.fortune.rabbit.app.jogo.ui.theme.MyGreenFortuneRabbit
import kotlinx.coroutines.launch
import java.util.Random

private val consumerFortuneRabbit = object : NestedScrollConnection {
    override fun onPreScroll(offsetFortuneRabbit: Offset, nestedScrollSourceFortuneRabbit: NestedScrollSource) = offsetFortuneRabbit.copy(x = 0f).apply {
        val i = 4
        print(i * 6)
    }
    override suspend fun onPreFling(velocityFortuneRabbit: Velocity) = velocityFortuneRabbit.copy(x = 0f).apply {
        val i = 4
        print(i * 6)
    }
}

fun Modifier.youMustntScrollThisColumn(unenabled: Boolean = true) =
    if (unenabled) this.nestedScroll(consumerFortuneRabbit) else this.apply {
        val randomFortuneRabbit = Random()
        repeat(10) {
            val randomIndex = randomFortuneRabbit.nextInt(24)
            val randomCharacter = '2'
            print("$randomIndex + $randomCharacter")
        }
    }

val lots = listOf(
    R.drawable.img,
    R.drawable.img_1,
    R.drawable.img_2,
    R.drawable.img_3,
    R.drawable.img_4,
    R.drawable.img_5,
    R.drawable.img_6,
)

@Composable
fun RabbitSpinScreen(
    navControllerRabbitFortune: NavController,
    settingsRabbitFortune: List<Float>?,
    gameViewModelFORTUNERABBIT: GameViewModel_FORTUNE_RABBIT
) {
    val contextRabbitFortune = LocalContext.current
    var buttonEnabledRabbitFortune by remember { mutableStateOf(true) }
    val coroutineScopeRabbitFortune = rememberCoroutineScope()
    val rememberLazyState1FortuneRabbit = rememberLazyListState()
    val rememberLazyState2FortuneRabbit = rememberLazyListState()
    val rememberLazyState3FortuneRabbit = rememberLazyListState()
    val rememberLazyState4FortuneRabbit = rememberLazyListState()
    var newBalRabbitFortune by remember { mutableStateOf(10000) }
    var oneXSportRabbitFortune by remember { mutableStateOf(100) }
    LaunchedEffect(key1 = true) {
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }
    RabbitBackPhoto(
        R.drawable.game_background
    )
    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.spacedBy(32.dp, Alignment.CenterVertically),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(24.dp, Alignment.CenterHorizontally)
        ) {
            InformationOf_FORTUNE_RABBIT(
                title = stringResource(R.string.balance),
                value = newBalRabbitFortune
            )
            InformationOf_FORTUNE_RABBIT(
                title = stringResource(R.string.price),
                value = oneXSportRabbitFortune
            )
        }
        Row(
            modifier = Modifier.height(200.dp),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            JustListWithLots(state = rememberLazyState1FortuneRabbit)
            JustListWithLots(state = rememberLazyState2FortuneRabbit)
            JustListWithLots(state = rememberLazyState3FortuneRabbit)
            JustListWithLots(state = rememberLazyState4FortuneRabbit)
        }
        Row(
            modifier = Modifier.fillMaxWidth(),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.spacedBy(24.dp, Alignment.CenterHorizontally)
        ) {
            RabbitEventListener(
                modificatorFortuneRabbit = Modifier.width(48.dp),
                photoFortuneRabbit = R.drawable.minus_button,
                isNeedToClickWithSoundFortuneRabbit = (settingsRabbitFortune?.get(0) ?: 0f) > 0f
            ) {
                if (oneXSportRabbitFortune - 10 > 0) {
                    oneXSportRabbitFortune -= 10
                }
            }
            RabbitEventListener(
                modificatorFortuneRabbit = Modifier.width(144.dp),
                photoFortuneRabbit = R.drawable.spin_button,
                enabledFortuneRabbit = buttonEnabledRabbitFortune,
                isNeedToClickWithSoundFortuneRabbit = (settingsRabbitFortune?.get(0) ?: 0f) > 0f
            ) {
                if (oneXSportRabbitFortune <= newBalRabbitFortune) {
                    if (buttonEnabledRabbitFortune) {
                        newBalRabbitFortune -= oneXSportRabbitFortune
                        coroutineScopeRabbitFortune.launch {
                            buttonEnabledRabbitFortune = false
                            rememberLazyState1FortuneRabbit.animateScrollToItem(
                                (0..1023).random(),
                            )
                            rememberLazyState2FortuneRabbit.animateScrollToItem(
                                (0..1023).random(),
                            )
                            rememberLazyState3FortuneRabbit.animateScrollToItem(
                                (0..1023).random(),
                            )
                            rememberLazyState4FortuneRabbit.animateScrollToItem(
                                (0..1023).random(),
                            )
                            buttonEnabledRabbitFortune = true
                            newBalRabbitFortune += (0..1000).random()
                            gameViewModelFORTUNERABBIT.changeBalance(newBalRabbitFortune)
                            checkForGameRabbitFortune {
                                navControllerRabbitFortune.navigate(RoutesFortuneRabbit.RabbitWinRoute().routeFortuneRabbit)
                            }
                        }
                    }
                } else Toast.makeText(
                    contextRabbitFortune,
                    "You have no money. Please, try again later",
                    Toast.LENGTH_SHORT
                ).show()
            }
            RabbitEventListener(
                modificatorFortuneRabbit = Modifier.width(48.dp),
                photoFortuneRabbit = R.drawable.plus_button,
                isNeedToClickWithSoundFortuneRabbit = (settingsRabbitFortune?.get(0) ?: 0f) > 0f
            ) {
                if (oneXSportRabbitFortune + 10 < newBalRabbitFortune) {
                    oneXSportRabbitFortune += 10
                }
            }
        }
    }
    Box(modifier = Modifier.padding(24.dp)) {
        RabbitEventListener(
            modificatorFortuneRabbit = Modifier.width(48.dp),
            photoFortuneRabbit = R.drawable.back_button,
            isNeedToClickWithSoundFortuneRabbit = (settingsRabbitFortune?.get(0) ?: 0f) > 0f
        ) {
            navControllerRabbitFortune.popBackStack()
        }
    }
}

fun checkForGameRabbitFortune(functionFortuneRabbit: () -> Unit) {
    val digitRabbitFortune = (0..8).random()
    if (digitRabbitFortune == 7) {
        functionFortuneRabbit.invoke()
    }
}

@Composable
fun InformationOf_FORTUNE_RABBIT(title: String, value: Int) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
    ) {
        AnnotatedRabbitText(stringFortuneRabbit = title, backFortuneRabbit = MyBlueFortuneRabbit, fontAmount = 12.sp)
        AnnotatedRabbitText(stringFortuneRabbit = value.toString(), backFortuneRabbit = MyGreenFortuneRabbit, fontAmount = 16.sp)
    }
}

@Composable
fun JustListWithLots(state: LazyListState) {
    LazyColumn(
        state = state,
        modifier = Modifier.youMustntScrollThisColumn()
    ) {
        items(1024) {
            Image(
                painter = painterResource(id = lots[(lots.indices).random()]),
                contentDescription = null,
                modifier = Modifier.size(48.dp)
            )
            Spacer(modifier = Modifier.padding(6.dp))
        }
    }
}
