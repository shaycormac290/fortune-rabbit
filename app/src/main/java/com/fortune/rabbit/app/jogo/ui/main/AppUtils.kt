package com.fortune.rabbit.app.jogo.ui.main

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.font.Font
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.sp
import com.fortune.rabbit.app.jogo.R

@Composable
fun RabbitBackPhoto(
    idFortuneRabbit: Int
) {
    Image(
        painter = painterResource(id = idFortuneRabbit),
        contentDescription = null,
        modifier = Modifier.fillMaxSize(),
        contentScale = ContentScale.Crop
    )
    LaunchedEffect(key1 = true) {
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }
}

@Composable
fun AnnotatedRabbitText(
    modifierFortuneRabbit: Modifier? = Modifier,
    stringFortuneRabbit: String,
    fontAmount: TextUnit? = null,
    backFortuneRabbit: Color? = null,
    aligningFortuneRabbit: TextAlign = TextAlign.Center
) {
    Text(
        modifier = modifierFortuneRabbit ?: Modifier,
        color = backFortuneRabbit ?: Color.White,
        text = stringFortuneRabbit,
        fontFamily = FontFamily(Font(R.font.kadwa)),
        fontSize = fontAmount ?: 16.sp,
        overflow = TextOverflow.Ellipsis,
        maxLines = 7,
        textAlign = aligningFortuneRabbit
    )
    LaunchedEffect(key1 = true) {
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }
}

@Composable
fun RabbitEventListener(
    modificatorFortuneRabbit: Modifier = Modifier.fillMaxWidth(.6f),
    photoFortuneRabbit: Int,
    enabledFortuneRabbit: Boolean = true,
    isNeedToClickWithSoundFortuneRabbit: Boolean,
    onEventFortuneRabbit: () -> Unit
) {
    Image(
        painter = painterResource(id = photoFortuneRabbit),
        contentDescription = null,
        modifier = modificatorFortuneRabbit
            .clip(RoundedCornerShape(20))
            .clickable {
                if (isNeedToClickWithSoundFortuneRabbit) SoundExecutor_FORTUNE_RABBIT.startProcessing_FORTUNE_RABBIT()
                onEventFortuneRabbit.invoke()
            },
        contentScale = ContentScale.FillWidth
    )
    LaunchedEffect(key1 = true) {
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
    }
}
