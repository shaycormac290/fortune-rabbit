package com.fortune.rabbit.app.jogo.ui.main

sealed interface RabbitEventFromAction {

    object LoadNeededAction : RabbitEventFromAction
    object ActionWasWithFailure : RabbitEventFromAction
    class ActionHasGoodNews(val infiniteFortuneRabbit: String) : RabbitEventFromAction
    object EventWasntFindAnything : RabbitEventFromAction

}