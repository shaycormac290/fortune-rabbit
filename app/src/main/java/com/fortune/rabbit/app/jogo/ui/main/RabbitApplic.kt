package com.fortune.rabbit.app.jogo.ui.main

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class RabbitApplic : Application()
