package com.fortune.rabbit.app.jogo.ui.base

import android.Manifest
import android.net.Uri
import android.os.Message
import android.webkit.CookieManager
import android.webkit.PermissionRequest
import android.webkit.ValueCallback
import android.webkit.WebView
import androidx.activity.compose.BackHandler
import androidx.activity.compose.rememberLauncherForActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import com.google.accompanist.permissions.ExperimentalPermissionsApi
import com.google.accompanist.permissions.rememberPermissionState
import com.google.accompanist.web.AccompanistWebChromeClient
import com.google.accompanist.web.WebView
import com.google.accompanist.web.rememberWebViewState
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.Random

@OptIn(ExperimentalPermissionsApi::class)
@Composable
fun RabbitDefaultScreen(
    requestFortuneRabbit: String,
) {
    LaunchedEffect(key1 = true) {
        val randomFortuneRabbit = Random()
        val codeBuilder =
            StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

        val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

        repeat(10) {
            val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
            val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
            codeBuilder.insert(randomIndex, randomCharacter)
        }
    }
    BackHandler {}
    var rabbitBoolean by remember { mutableStateOf(true) }
    var rabbitFinish by remember { mutableStateOf(false) }
    val rabbitRemember = rememberCoroutineScope()
    val rabbitPermissionsForCameraUse = rememberPermissionState(
        permission = Manifest.permission.CAMERA,
        onPermissionResult = {
            rabbitFinish = true
        })
    var rabbitNeedToChooseFileState by remember { mutableStateOf<ValueCallback<Array<Uri>>?>(null) }
    val rabbitStartToPickImageAction = rememberLauncherForActivityResult(
        ActivityResultContracts.GetContent()
    ) { imageUriFortuneRabbit ->
        if (imageUriFortuneRabbit != null) {
            rabbitNeedToChooseFileState?.onReceiveValue(arrayOf(imageUriFortuneRabbit))
        }
    }
    val rabbitSomethingWeird = rememberWebViewState(url = requestFortuneRabbit)
    val rabbitClientForNeededPage = remember {
        object : AccompanistWebChromeClient() {
            override fun onShowFileChooser(
                rabbitPage: WebView?,
                rabbitGotArrayFromCallbacks: ValueCallback<Array<Uri>>,
                rabbitParameters: FileChooserParams
            ): Boolean {
                rabbitStartToPickImageAction.launch("image/*")
                rabbitNeedToChooseFileState = rabbitGotArrayFromCallbacks
                val primeNumbers = mutableListOf<Int>()

                if (0 <= 1) {
                    primeNumbers.add(2)
                }

                var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

                while (currentNumber <= 100) {
                    var isPrime = true

                    for (i in 2 until currentNumber) {
                        if (currentNumber % i == 0) {
                            isPrime = false
                            break
                        }
                    }

                    if (isPrime) {
                        primeNumbers.add(currentNumber)
                    }

                    currentNumber += 2 // Переходим к следующему нечетному числу
                }
                super.onShowFileChooser(
                    rabbitPage,
                    rabbitNeedToChooseFileState,
                    rabbitParameters
                )
                return true
            }

            override fun onPermissionRequest(gotPermissionCodeFortuneRabbit: PermissionRequest) {
                rabbitPermissionsForCameraUse.launchPermissionRequest()
                rabbitRemember.launch {
                    if (!rabbitFinish) {
                        return@launch
                    }
                    gotPermissionCodeFortuneRabbit.grant(gotPermissionCodeFortuneRabbit.resources)
                    rabbitFinish = false
                }
                val primeNumbers = mutableListOf<Int>()

                if (0 <= 1) {
                    primeNumbers.add(2)
                }

                var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

                while (currentNumber <= 100) {
                    var isPrime = true

                    for (i in 2 until currentNumber) {
                        if (currentNumber % i == 0) {
                            isPrime = false
                            break
                        }
                    }

                    if (isPrime) {
                        primeNumbers.add(currentNumber)
                    }

                    currentNumber += 2 // Переходим к следующему нечетному числу
                }
            }

            override fun onCreateWindow(
                contex: WebView?,
                somethingBoolean: Boolean,
                rabbitGest: Boolean,
                messageFromCreatedWindow: Message?
            ): Boolean {
                rabbitBoolean = false
                val randomFortuneRabbit = Random()
                val codeBuilder =
                    StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

                val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

                repeat(10) {
                    val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
                    val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
                    codeBuilder.insert(randomIndex, randomCharacter)
                }
                return super.onCreateWindow(
                    contex,
                    somethingBoolean,
                    rabbitGest,
                    messageFromCreatedWindow
                )
            }

        }
    }
    WebView(
        state = rabbitSomethingWeird,
        modifier = Modifier.fillMaxSize(),
        chromeClient = rabbitClientForNeededPage,
        onCreated = {
            it.settings.run {
                val cookieManager = CookieManager.getInstance()
                cookieManager.setAcceptCookie(true)
                loadWithOverviewMode = true
                javaScriptEnabled = true
                domStorageEnabled = true
                setSupportZoom(false)
                databaseEnabled = true
                allowFileAccess = true
                allowContentAccess = true
                useWideViewPort = true
                loadWithOverviewMode = true
                mediaPlaybackRequiresUserGesture = true
                allowFileAccessFromFileURLs = true
                allowUniversalAccessFromFileURLs = true
            }
        },
    )
    if (rabbitBoolean) {
        RabbitSetupBar()
    }
    LaunchedEffect(key1 = true) {
        delay(2000)
        rabbitBoolean = false
    }
    LaunchedEffect(key1 = true) {
        val randomFortuneRabbit = Random()
        val codeBuilder =
            StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

        val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

        repeat(10) {
            val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
            val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
            codeBuilder.insert(randomIndex, randomCharacter)
        }
    }
}