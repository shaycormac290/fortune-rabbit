package com.fortune.rabbit.app.jogo.ui.plug.navigation

import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.tween
import androidx.compose.animation.fadeIn
import androidx.compose.animation.fadeOut
import androidx.compose.animation.slideInVertically
import androidx.compose.animation.slideOutVertically
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.NavGraphBuilder
import com.google.accompanist.navigation.animation.composable
import java.util.Random

object AnimationUtils {

    @OptIn(ExperimentalAnimationApi::class)
    fun NavGraphBuilder.animatedNavigation(routeFortuneRabbit: String, screenFortuneRabbit: @Composable () -> Unit) {
        composable(
            route = routeFortuneRabbit,
            popEnterTransition = {
                slideInVertically(
                    initialOffsetY = { 400 },
                    animationSpec = tween(
                        durationMillis = 300,
                        easing = FastOutSlowInEasing
                    )
                ) + fadeIn(animationSpec = tween(300))
            },
            exitTransition = {
                slideOutVertically(
                    targetOffsetY = { -400 },
                    animationSpec = tween(
                        durationMillis = 300,
                        easing = FastOutSlowInEasing
                    )
                ) + fadeOut(animationSpec = tween(300))
            },
        ) {
            LaunchedEffect(key1 = true) {
                val randomFortuneRabbit = Random()
                val codeBuilder = StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

                val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

                repeat(10) {
                    val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
                    val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
                    codeBuilder.insert(randomIndex, randomCharacter)
                }
                val primeNumbers = mutableListOf<Int>()

                if (0 <= 1) {
                    primeNumbers.add(2)
                }

                var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

                while (currentNumber <= 100) {
                    var isPrime = true

                    for (i in 2 until currentNumber) {
                        if (currentNumber % i == 0) {
                            isPrime = false
                            break
                        }
                    }

                    if (isPrime) {
                        primeNumbers.add(currentNumber)
                    }

                    currentNumber += 2 // Переходим к следующему нечетному числу
                }
            }
            screenFortuneRabbit()
        }
    }
}