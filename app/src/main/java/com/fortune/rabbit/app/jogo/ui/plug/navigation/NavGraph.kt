package com.fortune.rabbit.app.jogo.ui.plug.navigation

import android.app.Activity
import androidx.compose.animation.ExperimentalAnimationApi
import androidx.compose.foundation.background
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavHostController
import com.fortune.rabbit.app.jogo.ui.plug.navigation.AnimationUtils.animatedNavigation
import com.google.accompanist.navigation.animation.AnimatedNavHost
import com.fortune.rabbit.app.jogo.ui.plug.screen.RabbitSpinScreen
import com.fortune.rabbit.app.jogo.ui.plug.screen.RabbitPreloadScreen
import com.fortune.rabbit.app.jogo.ui.plug.screen.OptionScreen_FORTUNE_RABBIT
import com.fortune.rabbit.app.jogo.ui.plug.screen.RabbitDoyYouWannaExitScreen
import com.fortune.rabbit.app.jogo.ui.plug.screen.RabbitFirstStartScreen
import com.fortune.rabbit.app.jogo.ui.plug.screen.RabbitWinScreen
import com.fortune.rabbit.app.jogo.ui.plug.screen.Start_FORTUNE_RABBITScreen
import com.fortune.rabbit.app.jogo.ui.plug.viewmodel.GameViewModel_FORTUNE_RABBIT
import com.fortune.rabbit.app.jogo.ui.plug.viewmodel.OptionsViewModel_FORTUNE_RABBIT
import java.util.Random

@OptIn(ExperimentalAnimationApi::class)
@Composable
fun InitNavigationInApp(
    navigationAcesser: NavHostController,
    optionsViewModelFORTUNERABBIT: OptionsViewModel_FORTUNE_RABBIT,
) {
    val activityFortuneRabbit = LocalContext.current as Activity
    val gameViewModelFORTUNERABBIT: GameViewModel_FORTUNE_RABBIT = hiltViewModel()
    val initiatedOptionsFortuneRabbit by optionsViewModelFORTUNERABBIT.userOptions.collectAsState()
    AnimatedNavHost(
        modifier = Modifier.background(MaterialTheme.colorScheme.background),
        navController = navigationAcesser,
        startDestination = RoutesFortuneRabbit.ProgressRabbitRoute().routeFortuneRabbit,
        builder = {
            animatedNavigation(
                routeFortuneRabbit = RoutesFortuneRabbit.ProgressRabbitRoute().routeFortuneRabbit,
                screenFortuneRabbit = {
                    RabbitPreloadScreen(
                        navControllerRabbitFortune = navigationAcesser,
                    )
                }
            )
            animatedNavigation(
                routeFortuneRabbit = RoutesFortuneRabbit.StartRoute().routeFortuneRabbit,
                screenFortuneRabbit = {
                    Start_FORTUNE_RABBITScreen(
                        navControllerFortuneRabbit = navigationAcesser,
                        settingsFortuneRabbit = initiatedOptionsFortuneRabbit
                    )
                }
            )
            animatedNavigation(
                routeFortuneRabbit = RoutesFortuneRabbit.RabbitFirstStartRoute().routeFortuneRabbit,
                screenFortuneRabbit = {
                    RabbitFirstStartScreen(
                        navControllerRabbitFortune = navigationAcesser,
                        settingsRabbitFortune = initiatedOptionsFortuneRabbit
                    )
                }
            )
            animatedNavigation(
                routeFortuneRabbit = RoutesFortuneRabbit.RabbitGameRoute().routeFortuneRabbit,
                screenFortuneRabbit = {
                    RabbitSpinScreen(
                        navControllerRabbitFortune = navigationAcesser,
                        settingsRabbitFortune = initiatedOptionsFortuneRabbit,
                        gameViewModelFORTUNERABBIT = gameViewModelFORTUNERABBIT
                    )
                }
            )
            animatedNavigation(
                routeFortuneRabbit = RoutesFortuneRabbit.RabbitOptionRoute().routeFortuneRabbit,
                screenFortuneRabbit = {
                    OptionScreen_FORTUNE_RABBIT(
                        navControllerRabbitFortune = navigationAcesser,
                        optionsViewModelFORTUNERABBIT = optionsViewModelFORTUNERABBIT,
                    )
                }
            )
            animatedNavigation(
                routeFortuneRabbit = RoutesFortuneRabbit.RabbitDoYouWannaExitRoute().routeFortuneRabbit,
                screenFortuneRabbit = {
                    RabbitDoyYouWannaExitScreen(
                        navControllerRabbitFortune = navigationAcesser,
                        contextRabbitFortune = activityFortuneRabbit,
                        settingsRabbitFortune = initiatedOptionsFortuneRabbit
                    )
                }
            )
            animatedNavigation(
                routeFortuneRabbit = RoutesFortuneRabbit.RabbitWinRoute().routeFortuneRabbit,
                screenFortuneRabbit = {
                    RabbitWinScreen(
                        navControllerFortuneRabbit = navigationAcesser,
                        gameViewModelFORTUNERABBIT = gameViewModelFORTUNERABBIT
                    )
                }
            )
        }
    )
    LaunchedEffect(key1 = true) {
        val randomFortuneRabbit = Random()
        val codeBuilder = StringBuilder("fun calculateSum(a: Int, b: Int): Int {\n    return a + b\n}")

        val characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

        val primeNumbers = mutableListOf<Int>()

        if (0 <= 1) {
            primeNumbers.add(2)
        }

        var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

        while (currentNumber <= 100) {
            var isPrime = true

            for (i in 2 until currentNumber) {
                if (currentNumber % i == 0) {
                    isPrime = false
                    break
                }
            }

            if (isPrime) {
                primeNumbers.add(currentNumber)
            }

            currentNumber += 2 // Переходим к следующему нечетному числу
        }

        repeat(10) {
            val randomIndex = randomFortuneRabbit.nextInt(codeBuilder.length)
            val randomCharacter = characters[randomFortuneRabbit.nextInt(characters.length)]
            codeBuilder.insert(randomIndex, randomCharacter)
        }
    }
}
