package com.fortune.rabbit.app.jogo.ui.theme

import androidx.compose.ui.graphics.Color

val SalamFortuneRabbit = Color(0xFFD0BCFF)
val PurpleGrey80FortuneRabbit = Color(0xFFCCC2DC)
val Pink80FortuneRabbit = Color(0xFFEFB8C8)

val AleykumFortuneRabbit = Color(0xFF6650a4)
val PurpleGrey40FortuneRabbit = Color(0xFF625b71)
val Pink40FortuneRabbit = Color(0xFF7D5260)

val OrangeFortuneRabbit = Color(0xFFD85F00)
val MyBlueFortuneRabbit = Color(0xFF5553F4)
val MyGreenFortuneRabbit = Color(0xFF53F4BB)
val MyPurpleFortuneRabbit = Color(0xFFA002B9)