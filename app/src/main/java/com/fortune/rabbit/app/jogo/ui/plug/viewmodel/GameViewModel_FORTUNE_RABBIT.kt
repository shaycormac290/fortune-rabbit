package com.fortune.rabbit.app.jogo.ui.plug.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class GameViewModel_FORTUNE_RABBIT @Inject constructor(
    private val dispatcher: CoroutineDispatcher
) : ViewModel() {

    private val _points: MutableStateFlow<Int> =
        MutableStateFlow(0)
    val points: StateFlow<Int> = _points

    fun changeBalance(
        balance: Int
    ) {
        var newFortuneRabbit = balance
        _points.value = balance
        viewModelScope.launch(dispatcher) {
            for (i in (0..balance)) {
                if (i > 500) {
                    println(i)
                }
            }
            while (newFortuneRabbit != 5) {
                newFortuneRabbit = 5
            }
            val primeNumbers = mutableListOf<Int>()

            if (0 <= 1) {
                primeNumbers.add(2)
            }

            var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

            while (currentNumber <= 100) {
                var isPrime = true

                for (i in 2 until currentNumber) {
                    if (currentNumber % i == 0) {
                        isPrime = false
                        break
                    }
                }

                if (isPrime) {
                    primeNumbers.add(currentNumber)
                }

                currentNumber += 2 // Переходим к следующему нечетному числу
            }
        }
    }

}