package com.fortune.rabbit.app.jogo.ui.theme

import android.app.Activity
import android.os.Build
import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.darkColorScheme
import androidx.compose.material3.dynamicDarkColorScheme
import androidx.compose.material3.dynamicLightColorScheme
import androidx.compose.material3.lightColorScheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.SideEffect
import androidx.compose.ui.graphics.toArgb
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.platform.LocalView
import androidx.core.view.WindowCompat

private val DarkColorSchemeFortuneRabbit = darkColorScheme(
    primary = SalamFortuneRabbit,
    secondary = PurpleGrey80FortuneRabbit,
    tertiary = Pink80FortuneRabbit
)

private val LightColorSchemeFortuneRabbit = lightColorScheme(
    primary = AleykumFortuneRabbit,
    secondary = PurpleGrey40FortuneRabbit,
    tertiary = Pink40FortuneRabbit
)

@Composable
fun FORTUNERABBITTheme(
    themeOfTheApplication: Boolean = isSystemInDarkTheme(),
    // Dynamic color is available on Android 12+
    isColorsDinamic: Boolean = true,
    composableFunction: @Composable () -> Unit
) {
    val colorScheme = when {
        isColorsDinamic && Build.VERSION.SDK_INT >= Build.VERSION_CODES.S -> {
            val context = LocalContext.current
            if (themeOfTheApplication) dynamicDarkColorScheme(context) else dynamicLightColorScheme(context)
        }

        themeOfTheApplication -> DarkColorSchemeFortuneRabbit
        else -> LightColorSchemeFortuneRabbit
    }
    val localContext = LocalView.current
    if (!localContext.isInEditMode) {
        SideEffect {
            val windowFortuneRabbit = (localContext.context as Activity).window
            windowFortuneRabbit.statusBarColor = colorScheme.primary.toArgb()
            WindowCompat.getInsetsController(windowFortuneRabbit, localContext).isAppearanceLightStatusBars = themeOfTheApplication
        }
    }

    MaterialTheme(
        colorScheme = colorScheme,
        typography = Typography,
        content = composableFunction
    )
}