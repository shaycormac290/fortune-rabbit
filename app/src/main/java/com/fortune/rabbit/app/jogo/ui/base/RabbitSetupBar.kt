package com.fortune.rabbit.app.jogo.ui.base

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material.LinearProgressIndicator
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.graphics.StrokeCap
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.main.RabbitBackPhoto
import com.fortune.rabbit.app.jogo.ui.main.AnnotatedRabbitText
import com.fortune.rabbit.app.jogo.ui.theme.OrangeFortuneRabbit

@Composable
fun RabbitSetupBar() {
    RabbitBackPhoto(
        R.drawable.rabbit_back
    )
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .alpha(.5f)
    )
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(64.dp),
        contentAlignment = Alignment.BottomCenter
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            LinearProgressIndicator(
                modifier = Modifier.fillMaxWidth(.8f),
                color = OrangeFortuneRabbit,
                backgroundColor = Color.White,
                strokeCap = StrokeCap.Round
            )
            AnnotatedRabbitText(
                stringFortuneRabbit = stringResource(R.string.loading),
                backFortuneRabbit = Color.White,
                fontAmount = 20.sp,
                modifierFortuneRabbit = Modifier.fillMaxWidth(),
                aligningFortuneRabbit = TextAlign.Center
            )
        }
    }
}