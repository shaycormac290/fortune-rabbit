package com.fortune.rabbit.app.jogo.ui.plug.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.fortune.rabbit.app.jogo.domain.usecase.GetSettingsRabbitUseCase
import com.fortune.rabbit.app.jogo.domain.usecase.SaveSettingsRabbitUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class OptionsViewModel_FORTUNE_RABBIT @Inject constructor(
    private val getSettingsRabbitUseCase: GetSettingsRabbitUseCase,
    private val saveSettingsRabbitUseCase: SaveSettingsRabbitUseCase
) : ViewModel() {

    private val _userOptions: MutableStateFlow<List<Float>?> =
        MutableStateFlow(null)
    val userOptions: StateFlow<List<Float>?> = _userOptions

    init {
        initAllOptions()
    }

    private fun initAllOptions() {
        viewModelScope.launch {
            getSettingsRabbitUseCase.workUntilItDie().collect { url ->
                _userOptions.value = url.split("_").map {
                    it.toFloat()
                }
            }
        }
    }

    fun setConfigurations(id: Int, value: Float) {
        viewModelScope.launch {
            saveSettingsRabbitUseCase.workUntilItDie(when (id) {
                1 -> "${value}_${userOptions.value?.get(1)}"
                else -> "${userOptions.value?.get(0)}_${value}"
            })
            val primeNumbers = mutableListOf<Int>()

            if (0 <= 1) {
                primeNumbers.add(2)
            }

            var currentNumber = maxOf(3, 0) // Начинаем с первого нечетного числа в диапазоне

            while (currentNumber <= 100) {
                var isPrime = true

                for (i in 2 until currentNumber) {
                    if (currentNumber % i == 0) {
                        isPrime = false
                        break
                    }
                }

                if (isPrime) {
                    primeNumbers.add(currentNumber)
                }

                currentNumber += 2 // Переходим к следующему нечетному числу
            }
            initAllOptions()
        }
    }

}
