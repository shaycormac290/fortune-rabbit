package com.fortune.rabbit.app.jogo.ui.base

import androidx.compose.foundation.BorderStroke
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material3.ButtonDefaults
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.fortune.rabbit.app.jogo.R
import com.fortune.rabbit.app.jogo.ui.main.RabbitBackPhoto
import com.fortune.rabbit.app.jogo.ui.main.AnnotatedRabbitText

@Composable
fun MessageThatUserHasNoNetwork(
    onClickFortuneRabbit: () -> Unit
) {
    RabbitBackPhoto(
        R.drawable.rabbit_back
    )
    Box(
        modifier = Modifier
            .fillMaxSize()
            .background(Color.Black)
            .alpha(.5f)
    )
    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(64.dp),
        contentAlignment = Alignment.BottomCenter
    ) {
        Column(
            horizontalAlignment = Alignment.CenterHorizontally,
            verticalArrangement = Arrangement.Center
        ) {
            AnnotatedRabbitText(
                stringFortuneRabbit = stringResource(R.string.no_internet),
                backFortuneRabbit = Color.White,
                fontAmount = 20.sp,
                modifierFortuneRabbit = Modifier.fillMaxWidth(),
                aligningFortuneRabbit = TextAlign.Start
            )
            TextButton(
                onClick = { onClickFortuneRabbit.invoke() },
                shape = CircleShape,
                border = BorderStroke(1.dp, Color.White),
                colors = ButtonDefaults.buttonColors(containerColor = Color.Black)
            ) {
                AnnotatedRabbitText(
                    stringFortuneRabbit = stringResource(R.string.try_again),
                    backFortuneRabbit = Color.White,
                    fontAmount = 24.sp,
                )
            }
        }
    }
}