package com.fortune.rabbit.app.jogo.domain.usecase

import com.fortune.rabbit.app.jogo.domain.repository.ZapuskatorInterface
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import javax.inject.Inject
import javax.inject.Named

class GetSettingsRabbitUseCase  @Inject constructor(
    @Named("Settings") private val repositoryFortuneRabbit: ZapuskatorInterface,
    @Named("power") private val digitFortuneRabbit: Int,
    private val dispatcherFortuneRabbit: CoroutineDispatcher,
) {

    suspend fun workUntilItDie() =
        withContext(dispatcherFortuneRabbit) {
            repositoryFortuneRabbit.getAllNededConfigurations().also {
                println(digitFortuneRabbit * 23)
            }
        }
}
